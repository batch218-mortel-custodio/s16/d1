/*console.log("Hello world");*/

// Assignment Operation
	//Basic Assignment Operator (=)
let assignmentNumber= 8;
console.log("The value of the assignmentNumber variable is: " + assignmentNumber);

// Arithmetic Operation

let x = 200;
let y = 18;

let sum = x + y;
console.log("x: "+ x);
console.log("y: "+ y);
console.log("Result of addition operation: " + sum);


let difference = x - y;
console.log("Result of substraction operation: " + difference);

let product = x * y;
console.log("Result of multiplication operation: " + product);

let quotient = x / y;
console.log("Result of division operation: " + quotient);

let modulo = x % y;
console.log("Result of modulo operation: " + modulo);


// Continuation of assignment operator

// Addition Assignment Operator
	
// assignmentNumber = assignmentNumber + 2; //long method
// console.log(assignmentNumber);

//short method 
assignmentNumber += 2;
console.log("Result of " + assignmentNumber);


assignmentNumber -= 2;
console.log("Result of " + assignmentNumber);


assignmentNumber *= 2;
console.log("Result of " + assignmentNumber);


assignmentNumber /= 2;
console.log("Result of " + assignmentNumber);


let mdas = 1 + 2 - 3 * 4 / 5
console.log(mdas);

let pemdas = (1+(2-3)) * (4/5);
console.log(pemdas);

// Operators that or substract values by 1 and reassign the value of the variables where the increment (++)/decrement(--) was applied.

let z = 1;

//pre-increment
let increment = ++z
console.log("Result of pre-increment: " + increment); //2
console.log("Result of pre-increment of z: " + z); //2

increment = z++
console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z); //3

/*increment = z++;
console.log("Result of post-increment: " + increment); //3
console.log("Result of post-increment of z: " + z); //4
*/

//pre is -- or ++ first before variable
//post is  variable first before -- or ++ 

let decrement = --z;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); //2

decrement = z--;
console.log("Result of post-decrement: " + decrement); //2
console.log("Result of post-dencrement of z: " + z); //1


//Type coercion is the automatic conversion of values from  one data type to another

let numA = 10;
let numB = true;

let coercion = numA+numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numX = 10;
let numY = "12"
coercion = numX + numY;
console.log(coercion);
console.log(typeof coercion);

let num1 = 1;
let num2 = false;

coercion = num1+num2;
console.log(coercion);
console.log(typeof coercion);

// Comparison Operators are used to evaluate the left and right operands. After evaluation, it returns a boolean value. it ignores data type. 

console.log(1 == 1);
console.log(1 == 2);

console.log(1 == '1'); //ignores data type, which makes this True
console.log(1 === '1'); //strict equality, follows data type, so False

console.log(0 == false); // true // false value is also equal to zero as proven above.


console.log("------------------");
let juan = "juan";
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); // false
console.log(juan == "juan"); //true

//inequality operator (!=)
console.log("------------------");
console.log(1 != 1); //false
console.log(1 != 2); // true
console.log(1 != '1');  //false
console.log(1 != false); //true

//Relational Operators - some comparison operators to check whether one value is greater or less than the other value

let a = 50;
let b = 65;
console.log("------------------");
console.log(a > b);

//GreaterThan Operator (>)

let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);

//LessThan Operator (<)

let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan);

let isGTorEqual = a >= b; //OR operators only need one of the two to satisfy for the boolean to become TRUE
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);


//AND  operator (&&)

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet =  isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " +allRequirementsMet);
// AND operator requires both are true


//OR operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " +someRequirementsMet);
//OR operator requires only 1 true

//NOT Operator (!)
let someRequirementsNotMet = !isLegalAge;
console.log("Result of logical NOT Operator: " +someRequirementsNotMet);
//NOT Operator returns the opposite value (if true --> false)

